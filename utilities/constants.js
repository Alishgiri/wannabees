const placeName = "Wannabees Family Play Town";
const encodedPlaceName = encodeURIComponent(placeName);
const GOOGLE_API_KEY = "AIzaSyDzTKfXriC3WlUsTutjDbOxk50A0Ifx2LU";

const BASE_URL = "https://maps.googleapis.com/maps/api/place/";
export const GET_PLACE_ID_URL = `${BASE_URL}findplacefromtext/json?input=${encodedPlaceName}&inputtype=textquery&fields=place_id,name,opening_hours&key=${GOOGLE_API_KEY}`;

export function constructOpeningHoursURLWith(placeId) {
  return `${BASE_URL}details/json?placeid=${placeId}&fields=opening_hours&key=${GOOGLE_API_KEY}`;
}
