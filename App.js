import { Navigation } from "react-native-navigation";
import Icon from "react-native-vector-icons/Ionicons";

import HomeView from "./src/screens/HomeView";
import OpeningHoursView from "./src/screens/OpeningHoursView";
import { HOME_SCREEN, OPENING_HOURS } from "./src/screens/constants";

Navigation.registerComponent(HOME_SCREEN, () => HomeView);
Navigation.registerComponent(OPENING_HOURS, () => OpeningHoursView);

Navigation.events().registerAppLaunchedListener(() => {
  Promise.all([
    Icon.getImageSource("ios-home", 18),
    Icon.getImageSource("ios-time", 18)
  ]).then(icon => {
    Navigation.setRoot({
      root: {
        bottomTabs: {
          children: [
            {
              stack: {
                children: [
                  {
                    component: {
                      name: HOME_SCREEN,
                      options: {
                        topBar: {
                          title: {
                            text: "Wannabees Family Play Town"
                          }
                        }
                      }
                    }
                  }
                ],
                options: {
                  topBar: {
                    title: {
                      text: "Wannabees Family Play Town"
                    }
                  },
                  bottomTab: {
                    text: "Home",
                    icon: icon[0],
                    fontSize: 14,
                    selectedTextColor: "#e5a722",
                    selectedIconColor: "#e5a722"
                  }
                }
              }
            },
            {
              stack: {
                children: [
                  {
                    component: {
                      name: OPENING_HOURS,
                      options: {
                        topBar: {
                          title: {
                            text: "Wannabees Family Play Town"
                          }
                        }
                      }
                    }
                  }
                ],
                options: {
                  topBar: {
                    title: {
                      text: "Wannabees Family Play Town"
                    }
                  },
                  bottomTab: {
                    text: "Opening Hours",
                    icon: icon[1],
                    fontSize: 14,
                    selectedTextColor: "#e5a722",
                    selectedIconColor: "#e5a722"
                  }
                }
              }
            }
          ]
        }
      }
    });
  });
});
