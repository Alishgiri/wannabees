import React from "react";
import { View, StyleSheet } from "react-native";

const SafeArea = ({ style, children }) => (
  <View style={[styles.container, style]}>
    <View style={styles.contentArea}>{children}</View>
  </View>
);

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  contentArea: {
    flex: 1,
    marginVertical: 20,
    marginHorizontal: 20
  }
});

export { SafeArea };
