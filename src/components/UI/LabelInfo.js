import React from "react";
import { View, Text, StyleSheet } from "react-native";

const LabelInfo = props => (
  <View style={styles.input_with_label}>
    <Text style={styles.label}>{props.label}: </Text>
    <Text style={styles.desc} {...props}>
      {props.desc}
    </Text>
  </View>
);

const styles = StyleSheet.create({
  input_with_label: {
    margin: 10,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between"
  }
});

export default LabelInfo;
