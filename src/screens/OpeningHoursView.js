import Axios from "axios";
import React from "react";
import { Navigation } from "react-native-navigation";
import { View, Text, StyleSheet } from "react-native";

import {
  GET_PLACE_ID_URL,
  constructOpeningHoursURLWith
} from "../../utilities/constants";
import { Spinner } from "../components/UI/Spinner";
import { SafeArea } from "../components/UI/SafeArea";

class OpeningHoursView extends React.Component {
  state = {
    isLoading: false,
    apiFetchError: false,
    scheduledOpeningTimes: null
  };

  constructor(props) {
    super(props);
    Navigation.events().bindComponent(this);
  }

  componentDidAppear() {
    this.setState({ isLoading: true });
    Axios.get(GET_PLACE_ID_URL)
      .then(response => {
        const placeId = response.data.candidates[0].place_id;
        Axios.get(constructOpeningHoursURLWith(placeId))
          .then(response => {
            const scheduledOpeningTimings =
              response.data.result.opening_hours.weekday_text;
            this.setState({
              isLoading: false,
              scheduledOpeningTimes: scheduledOpeningTimings
            });
          })
          .catch(error => {
            this.setState({ isLoading: false, apiFetchError: true });
          });
      })
      .catch(error => {
        this.setState({ isLoading: false, apiFetchError: true });
      });
  }

  renderOpeningHours = () => {
    if (this.state.scheduledOpeningTimes) {
      return (
        <SafeArea>
          <Text style={styles.header}>Opening Hours View</Text>
          <View style={styles.list_container}>
            {this.state.scheduledOpeningTimes.map((time, index) => {
              return (
                <Text key={index} style={styles.scheduled_timings}>
                  {time}
                </Text>
              );
            })}
          </View>
        </SafeArea>
      );
    }
    return <Spinner />;
  };

  render() {
    if (this.state.apiFetchError) {
      return (
        <View style={styles.network_error_msg_view}>
          <Text style={styles.error_text}>Network Error</Text>
        </View>
      );
    }
    return this.renderOpeningHours();
  }
}

const styles = StyleSheet.create({
  header: {
    fontSize: 18,
    fontWeight: "bold",
    textAlign: "center"
  },
  list_container: {
    margin: 10,
    padding: 5,
    borderWidth: 1,
    borderRadius: 5,
    borderColor: "#9da0a5"
  },
  scheduled_timings: {
    margin: 5,
    width: "80%",
    alignSelf: "flex-end"
  },
  network_error_msg_view: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  error_text: {
    color: "#af0000"
  }
});

export default OpeningHoursView;
