import React, { Component } from "react";
import { View, StyleSheet, Linking } from "react-native";
import { SafeArea } from "../components/UI/SafeArea";
import LabelInfo from "../components/UI/LabelInfo";

class HomeView extends Component {
  render() {
    return (
      <SafeArea>
        <View style={styles.container}>
          <LabelInfo label="Address" desc="Frenchs Forest" />
          <LabelInfo label="Phone Number" desc="+61 2 8021 6902" />
          <LabelInfo
            label="Website"
            style={{ color: "blue" }}
            desc="Wannabees Family Play Town"
            onPress={() => Linking.openURL("https://wannabees.com.au")}
          />
        </View>
      </SafeArea>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: "100%"
  }
});

export default HomeView;
